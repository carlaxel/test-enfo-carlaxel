const http = require('http');
const app = require('./app');
const connectToDatabase = require('./io').connect;

const PORT = process.env.PORT || 3000;  //declares PORT to either env varible or if undefinded to 3000

waitForConnection();


//Recursive function that repeatedly calls imaginary database with a delay until success i achieved
function waitForConnection () {
    connectToDatabase().then(()=>{
        //promise is resolved, start listening to PORT
        app.server = http.createServer(app);
        app.server.listen(PORT, () => {
            console.log(`API started on port ${app.server.address().port}!`);
        });
        
    }).catch(()=>{
        //Promise rejected, recursively call on function to retry after 1000ms timeout
        setTimeout(waitForConnection,1000)
    });
}


