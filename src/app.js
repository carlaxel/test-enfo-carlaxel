
const express = require('express');
const articles = require('./api/articles');
const bodyParser = require("body-parser"); 
const requestLogger = require("./api/loggers/requestLogger");

const app = express();


/*
    Body-parser parses the JSON data from the request object and maps it to req.body
*/

app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.json()); 
app.use(requestLogger); //Logs data from all request



// mount routers for each API resource
app.use('/articles', articles);

module.exports = app;
