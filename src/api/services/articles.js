const db = require('../../io').db;

module.exports = {

    getAll() {
        return db.getAllArticles();
    },
    postArticle(data){
        //function passes on the function from database file
        return db.insertArticle(data);
    }

    

}
