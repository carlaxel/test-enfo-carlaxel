const fs = require('fs');

function requestLogger(req,res,next){
    
    const {_hadError:hadError,parser} = req.socket; //deconstruct _hadError and parser, also rename _hadError
    const {url, method, params, query, body} = parser.incoming; //deconstruct from parser.incoming

    const requestLog = {
        url,
        method,
        hadError,
        incomingData:{
            params,
            query,
            body
        },
        timestamp: new Date()

    }
/*
    The request object collects all the data that we later will store in database
*/



    writeNewLog(requestLog); //reads prev data and pushes new post to JSON file
    
    

    next(); //next passes the operation to the next middleware
}

function writeNewLog(post){
 let responseDB = fs.readFileSync('src/api/loggers/requestLogger.json', 'utf-8');
 writeToFile(JSON.parse(responseDB), post)
}

function writeToFile(data, post){
    data.logs.push(post);
    fs.writeFile('src/api/loggers/requestLogger.json', JSON.stringify(data), function(err) {
        if (err) throw err;
      });
}

module.exports = requestLogger;