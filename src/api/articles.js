
const Router = require('express').Router;
const ArticlesService = require('./services/articles');

let router = Router();

/*
    Routes for /articles

        GET
        POST can include author,title, description, url, urlToImage    

*/

//Send articles, if successful otherwise error 502
router.get('/', (req, res) => {
    ArticlesService.getAll()
        .then(data => {
            console.log("success");
            res.status(200).json(data);
        }).catch(()=>{
            console.log("error");
            res.status(502).send("error 502");
        });
});

//Post articles, if successful otherwise error 502
router.post('/', (req, res) => {

    //create post object with default values if it is missing values
    const {author = null, title = null, description = null, url = null, urlToImage = null} = req.body;
    const post = {author,title,description,url,urlToImage, publishedAt: new Date()}

    ArticlesService.postArticle(post).then(()=>{
        res.status(200).send("OK")
    }).catch(()=>{
        res.status(502).send("Error 502")
    })
    
});

module.exports = router;
