
const fs = require('fs');

/**
 *  NOTE: This file pretends to connect to and use a database. You don't have to modify it.
 *  That said: it might help to tweak the connection success rate for testing purposes.
 *
 **/

const CONNECTION_SUCCESS_RATE = 0.90;

let isConnected = false;

function connectSuccess() {
    return Math.random() < CONNECTION_SUCCESS_RATE;
}

function saveArticle(data) { //Reads data from JSON file and pushed new post and then writes to JSON file
    let postedArticles = loadArticles();
    postedArticles.articles.push(data); 

    fs.writeFile('src/io/articles.json', JSON.stringify(postedArticles), function(err) {
        if (err) throw err;
      });
}

function loadArticles() { //reads data from JSON sync and returns object
    let data = fs.readFileSync('src/io/articles.json', 'utf-8') 
        return JSON.parse(data);
}

const db = {
    getAllArticles: () => {
        if (isConnected) {

            const data = loadArticles();
            return Promise.resolve(data);
            
        }
        return Promise.reject(new Error('No connection to database.'));
    },
    insertArticle: (data) => {
        if (isConnected) {
            saveArticle(data);
            return Promise.resolve('OK');
        }
        return Promise.reject(new Error('No connection to database.'));
    }
}

function connect() {
    return new Promise((resolve, reject) => setTimeout(() => {
        if (connectSuccess()) {
            console.log('Connection to db OK.');
            isConnected = true;
            resolve()
        } else {
            console.error('Connection to db FAILED.');
            isConnected = false;
            reject();
        }
    }, 1000));
}

module.exports = {
    db,
    connect,

}
