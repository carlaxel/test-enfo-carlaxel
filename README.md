# Technical test - Node/Express - "Enfo World News"

## Goal and Evaluation
The purpose of this test is to show your aptitude with the Node/Express framework and JavaScript. We'd like you to, as far as possible, show your thinking by writing relevant comments to the code you modify or add. You're expected to follow a similar code style and to motivate the installation of additional dependencies.

## Level
The exercises are listed in increasing difficulty and meant for junior developers.

## Requirements
- Node (currently using Node 8 but any version 6+ should be fine)
- NPM
- Git
- A code editor

## Exercises

1. Clone this repository and install the dependencies.
3. Add a `npm start` script to `package.json` that starts the API on port 3000.
2. There are some hardcoded values and magic numbers in the code, like the port Node will open to listen for incoming connections. Improve the code so it's possible to set these variables using environment variables.
5. Improve the router for `/articles` to send error code 502 if data couldn't be fetched from the database.
4. Write a simple middleware to log all API requests. Use information from the request object and any system information you find relevant.
6. Improve the router and service for `/articles` to make it possible to add more articles by sending JSON objects as POST requests to `/articles`. (Hint: you might need to install a middleware to handle POST request with JSON bodies).
7. There's a small chance that the database connection will fail when the API starts. Improve the start up code so that the server doesn't start to listen for connections before the connection has been established.
8. Extend the fake database to save added articles to a file so they don't disappear when the server restarts.

### Good luck!
